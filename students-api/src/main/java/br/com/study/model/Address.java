package br.com.study.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Address {

	private String city;

	private String street;

	private Long number;

	public Address(String city) {
		this.city = city;
	}

}
