package br.com.study.config.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ApiRestExceptionHandler {

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { MethodArgumentNotValidException.class })
	public List<ApiError> badRequest(MethodArgumentNotValidException exception) {
		List<ApiError> apiErrors = new ArrayList<>();
		List<FieldError> fieldError = exception.getBindingResult().getFieldErrors();

		fieldError.forEach(er -> {
			ApiError error = new ApiError(er.getField(), er.getDefaultMessage(), er.getCode());

			apiErrors.add(error);
		});

		log.error("ApiRestExceptionHandler: Status " + HttpStatus.BAD_REQUEST);

		return apiErrors;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = { EmptyResultDataAccessException.class })
	public ApiError internalServerError(EmptyResultDataAccessException exception) {
		String message = exception.getMessage();

		ApiError apiError = new ApiError();

		apiError.setMessage(message);

		log.error("ApiRestExceptionHandler: Status " + HttpStatus.INTERNAL_SERVER_ERROR);

		return apiError;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = { NoSuchElementException.class })
	public ApiError internalServerError(NoSuchElementException exception) {
		ApiError apiError = new ApiError(exception.getMessage());

		log.error("ApiRestExceptionHandler: Status " + HttpStatus.INTERNAL_SERVER_ERROR + apiError.getMessage());

		return apiError;
	}

}
