package br.com.study.resource.dto;

import org.springframework.data.domain.Page;

import br.com.study.model.Student;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class StudentDto {
	
	private Long id;

	private String name;

	private Integer age;

	public StudentDto(Student student) {
		this.id = student.getId();
		this.name = student.getName();
		this.age = student.getAge();
	}

	public static Page<StudentDto> converter(Page<Student> listStudents) {
		return listStudents.map(StudentDto::new);
	}

}
